/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gipltraining.switchcasedemo;

/**
 *
 * @author Admin
 */
public class SwitchCaseDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
      String country="India";
      switch(country)
      {
          case "Australia":
              System.out.println("Australia is an island continent and the world's sixth largest country");
              break;
               case "America":
              System.out.println("America, is a country primarily located in North America.");
              break;
               case "china":
              System.out.println(" China is the world's most populous country, with a population of more than 1.4 billion");
              break;
               case "India":
              System.out.println("India is the seventh-largest country by area, the second-most populous country, and the most populous democracy in the world");
              break;
                 case "Finland":
              System.out.println("Finland is a Northern European nation bordering Sweden, Norway and Russia. Its capital, Helsinki, occupies a peninsula and surrounding islands in the Baltic Sea");
              break;  
                 default:
              System.out.println("No match");
      }
    }
    
}
