/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gipltraining.elseifdemo;

/**
 *
 * @author Admin
 */
public class ElseIfDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
      int percentage=60;
      if(percentage>75)
      {
          System.out.println("Distinction");
      }
      else if(percentage>60)
      {
          System.out.println("First class");
      }
      else if(percentage>50)
      {
          System.out.println("Second class");
      }
      else if (percentage>35)
      {
          System.out.println("Third class");
      }
    }
    
}
