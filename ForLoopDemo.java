/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gipltraining.forloopdemo;

/**
 *
 * @author Admin
 */
public class ForLoopDemo {

    /**
     * @param args the command line arguments
     */
	public static void main(String args[])
	{
	 int a,b,c,n=10;     //n=10 mean first 10 fibonacci numbers
          a=0;
          b=1;
          System.out.print(a+" "+b+" "); 
          for(int i=2;i<n;i++)
	    {	
               c=a+b;
               a=b;
               b=c;
	       System.out.print(c+" "); 
            }
       }
 }
    

